#!/usr/bin/python2.6
# This file is part of ErnChord
#
# ErnChord is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) 2010 Vourlakis Nikolas

"""
Module chord/chord_interface.py: Top level module to control a chord node
Classes: ChordInterface
Functions: ChordInterface
Status: READY & TESTED
"""

__author__ = "nvourlakis@gmail.com (Vourlakis Nikolas)"
__version__ = "1.0"


# Standard module imports
import logging
from hashlib import sha1

# self-defined module imports
import net.chord_node as chord_node
import net.sim_controller as sim_controller
from net.data.url import Url
import net.protobuf


# Logging facility
log = logging.getLogger("__name__")
log.setLevel(logging.INFO)
loghand = logging.StreamHandler()
logform = logging.Formatter("%(levelname)s [%(module)s] in %(funcName)s at %(lineno)d-> %(message)s")
logform = logging.Formatter("%(levelname)s: %(message)s")
loghand.setFormatter(logform)
log.addHandler(loghand)


class ChordInterface(object):
    """
    Class ChordInterface

    ChordInterface exposes to the client of the ErnChord only the functionality
    he needs.
    """

    def __init__(self, port, hostname, is_bootstrap = False):
        self.chord =chord_node.ChordNode(port, hostname)

        self._is_bootstrap = is_bootstrap

        if is_bootstrap:
            self._bootstrap = Url(port, hostname)
            self.chord.init_bootstrap()
        else:
            self._bootstrap = None
        self.chord._endpoint.run_endpoint()

    def connect(self, port, hostname):
        """
        Join the chord ring.
        """
        self._bootstrap = Url(port, hostname)
        self.chord.join(self._bootstrap)
        self.chord.strong_stabilize()

        # my_suc = self.chord._references.get_successor(0)
        # self.chord.ask_keys(dest = my_suc)

    def leave_network(self):
        self.chord._scheduler.StopAllTasks()
        self.chord._stop_executor = True

        if not self.chord._disconnected:
            self.chord.transfer_keys()
            self.chord._endpoint.shutdown()
            self.chord._disconnected = True
        else:
            self.chord._endpoint.shutdown()

    def find(self, search_term):
        """
        Search the chord ring for something!
        # FIXME: This is a temporary quick and dirty hack
        temporary....my ass
        """
        search_hash = long(sha1(search_term).hexdigest(), 16)

        my_pred = self.chord._references.get_predecessor()
        key_found = False
        result = None

        # Check to see if I hold the key
        result = self.chord._stored_keys.has_key(search_hash)
#        if search_hash > self.chord._local_url.hash_code() and \
#        search_hash > my_pred.hash_code():
#            result = self.chord._stored_keys.has_key(search_hash)
#        elif search_hash > self.chord._local_url.hash_code() and \
#        search_hash < my_pred.hash_code():
#            result = self.chord._stored_keys.has_key(search_hash)

        if result is None:
            to_contact = self.chord._references.search_finger_table(search_hash)

            if to_contact == self.chord._local_url:
                # in case we are back in history concerning the finger table
                to_contact = self.chord._references.get_successor(0)

            search_status = self.chord.search_key(search_hash, to_contact)

            if not search_status:
                result = "Key not found, last node contacted was %s" % to_contact
                return result

            result_dict = {}
            while len(self.chord._search_result) == 0:
                        continue
            result_dict = self.chord._search_result.pop()
            result = result_dict["search_result"]

            while not result_dict["resolved"]:
                to_contact = str_to_url(result_dict["search_result"])
                search_status = self.chord.search_key(search_hash, to_contact)

                if not search_status:
                    # rpc failure
                    result = "Key not found, last node contacted was %s" % to_contact
                    break

                while len(self.chord._search_result) == 0:
                    continue
                result_dict = self.chord._search_result.pop()
                result = result_dict["search_result"]

        return result

    def push(self, key_value_list):
        # push the local data to the chord ring
        for i in key_value_list:
            print "inserting:", i
            c.chord._stored_keys.insert(Key(i[0]), i[1])


def str_to_url(str_url):
    """
    Helper function that accepts a Url formatted as a string and returns an
    Url object. It is used mostly when we have a response from a rpc where
    the Url is stored as a string.
    """

    part = str_url.partition(':')
    url_obj = Url(int(part[2]), part[0])
    return url_obj


#if __name__ == "__main__":
#    c = ChordInterface(3000, "127.0.0.1", is_bootstrap = True)
