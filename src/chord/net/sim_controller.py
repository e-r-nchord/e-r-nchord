#!/usr/bin/python2.6
# This file is part of ErnChord
#
# ErnChord is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) 2010 Vourlakis Nikolas

"""
Module chord/net/sim_controller.py: This is the controller of the simulations
Classes: SimController
Functions: create_channel, create_controller, str_to_url
Status: READY & TESTED
"""

__author__ = "nvourlakis@gmail.com (Vourlakis Nikolas)"
__version__ = "1.0"


# Standard module imports
import random
import threading
import signal
from collections import deque
import logging

# self-defined module imports
from data.url import Url
from data.key import Key
from references import References
from stored_keys import StoredKeys
from endpoint import Endpoint
from callback import Callback
import rpc_services as services
import simulation_services
import task_scheduler

# protoc generated module imports
import service_pb2
import simulation_service_pb2

#3rd party module imports
import protobuf


# Logging facility
log = logging.getLogger("__name__")
log.setLevel(logging.INFO)
loghand = logging.StreamHandler()
logform = logging.Formatter("%(levelname)s [%(module)s] in %(funcName)s at %(lineno)d-> %(message)s")
loghand.setFormatter(logform)
log.addHandler(loghand)


class SimController:
    """
    Class SimController

    This class represents a node in the chord ring. It's purpose is to
    schedule and run the necessary tasks for the stability of the Chord ring as
    described in Chord paper.
    """

    def __init__(self, port, host = "127.0.0.1"):

        self._stored_keys = StoredKeys()
        # List containing all the urls of all the remote node in the chord ring
        self._remote_urls_list = []

        self._local_url = Url(port, host)
        self._on_cycle = False
        self._disconnected = True
        self._references = References(self._local_url)
        self._endpoint = Endpoint(port, host)
        self._search_result = deque() # buffer used when making a search
        self._buffer = deque() # buffer used mainly from find_successor

        self._queue_task = deque() # buffer used for functions that must be run
        self._worker_thread = threading.Thread(target = self.task_executor)
        self._worker_thread.start()

        # Create the schedule and add the tasks
        self._scheduler = task_scheduler.Scheduler()
        # the arguments are: task, loopdelay, initdelay
        # Pings every 2 secs and strong_stabilize every 8 secs.
        # This may change.
        self._scheduler.AddTask(self.task_successor_ping, 2, 0)
        self._scheduler.AddTask(self.task_predecessor_ping, 2, 0)
        self._scheduler.AddTask(self.task_strong_stabilize, 8, 0)

        # Initializing services
        self.finds_service = services.FindSuccessorService(self)
        self.pred_service = services.PredecessorService(self)
        self.sucs_service = services.SuccessorService(self)
        self.noti_service = services.NotifyService(self)
        self.ping_service = services.PingService()
        self.askK_service = services.AskKeysService(self)
        self.trans_service = services.TransferKeysService(self)
        self.sear_service = services.SearchService(self)

        # Services for simulation purposes
        self.crash_service = simulation_services.CrashService(self)
        self.leave_service = simulation_services.LeaveService(self)
        self.url_service = simulation_services.MyUrlService(self)

        self.__callback = Callback(self)

        # Registering services
        self._endpoint.registerService(self.finds_service)
        log.info("[SimController] FindSuccessorService registered")
        self._endpoint.registerService(self.noti_service)
        log.info("[SimController] NotifyService registered")
        self._endpoint.registerService(self.pred_service)
        log.info("[SimController] PredecessorService registered")
        self._endpoint.registerService(self.ping_service)
        log.info("[SimController] PingService registered")
        self._endpoint.registerService(self.sucs_service)
        log.info("[SimController] SuccessorService registered")
        self._endpoint.registerService(self.askK_service)
        log.info("[SimController] AskKeysService registered")
        self._endpoint.registerService(self.trans_service)
        log.info("[SimController] TransferKeysService registered")
        self._endpoint.registerService(self.sear_service)
        log.info("[SimController] SearchService registered")
        self._endpoint.registerService(self.crash_service) # simulation
        log.info("[SIMULATION] CrashService registered") # simulation
        self._endpoint.registerService(self.leave_service) # simulation
        log.info("[SIMULATION] LeaveService registered") # simulation
        self._endpoint.registerService(self.url_service) # simulation
        log.info("[SIMULATION] MyUrlService registered") # simulation
        log.info("[SimController] All services registered")

        # Start all scheduled tasks
        self._scheduler.StartAllTasks()

    def task_successor_ping(self):
        """
        This is a scheduled task that pings the successor. If he responds then
        all is ok. If not the local node sets as the new successor the 1st
        element from the successor list stored in the References. Then we
        have a strong stabilization, healing of the finger table and fixing the
        new successor list.
        Runs in a separate thread from task_scheduler.Scheduler
        """

        my_suc = self._references.get_successor(0)

        if self._disconnected:
            return

        if my_suc is None:
            return
        else:
            is_done = self.ping(my_suc)
            if not is_done:
                new_suc = self._references.get_successor_list()
                if new_suc[0] is None:
                    self._references.set_successor(self._local_url, 0)
                    self._references.set_successor(self._local_url, 1)
                else:
                    self._references.set_successor(new_suc[0], 0)
                    self._references.set_successor(new_suc[0], 1)
                    self._references.update_successor_list(None)
                self._queue_task.appendleft(self.strong_stabilize)
                self._queue_task.appendleft(self.build_fingers)
                self._queue_task.appendleft(self.fetch_successor_list)

    def task_predecessor_ping(self):
        """
        This is a scheduled task that pings the predecessor. If we don't get a
        response then the local node sets as the new predecessor the 1st
        element of the backward fingers (_back_fings) found in the References.
        Then we need to renew the backward fingers. We don't really care if
        it's the correct predecessor as this will be fixed the next time the node
        will stabilize. We only need somthing that can connect to and make some
        rpcs correctly.
        Runs in a separate thread from task_scheduler.Scheduler
        """

        if self._disconnected:
            return

        my_pred = self._references.get_predecessor()

        if my_pred is None or my_pred == self._local_url:
            return
        else:
            is_done = self.ping(my_pred)
            if not is_done:
                new_pred = self._references.get_back_fings()
                if new_pred[0] is None:
                    self._references.set_predecessor(self._local_url)
                else:
                    self._references.set_predecessor(new_pred[0])
                self._references.update_back_fings(None)
                self._queue_task.appendleft(self.fetch_backwards_fings)
                self._queue_task.appendleft(self.ask_keys)

    def task_strong_stabilize(self):
        """
        A scheduled task that runs strong stabilization.
        Runs in a separate thread from task_scheduler.Scheduler
        """

        if self._disconnected:
            return

        if self._references.get_successor(0) is None or \
        self._references.get_predecessor is None:
            return
        else:
            self.strong_stabilize()

    def task_executor(self):
        """
        The queue_task is checked and if there are some tasks (functions)
        then they are get run.
        Runs in a separate thread.
        """

        while True:
            if len(self._queue_task) != 0:
                task = self._queue_task.pop()
                log.debug("Executor is going to execute: %s" % str(task))
                task()

    # DEBUGGING PURPOSES ONLY
    def my_status(self):
        """
        Shows the status of the local node. This is for debugging purposes only.
        """
        log.info("\n+============== STATUS ==============+" +
        "\n+ My predecessor:" +
        "\n| \tpredecessor %s" % (self._references.get_predecessor()) +
        "\n+ My successors:" +
        "\n| \tsuccessor[0]: %s" % self._references.get_successor(0) +
        "\n| \tsuccessor[1]: %s" % self._references.get_successor(1) +
        "\n| My forward successor list:" +
        "\n+ \t%s" % [ str(i) for i in self._references.get_successor_list()] +
        "\n| My backward predecessor list:" +
        "\n+ \t%s" % [ str(i) for i in self._references.get_back_fings()] +
        "\n+ Am I on cycle? %s" % self._on_cycle +
        "\n+ My finger table is filled? %s" % (not self._references._fingers.is_empty()) +
        "\n+====================================+\n")

    #=========# BOOTSTRAP #=========#
    def init_bootstrap(self):
        """
        This function is inteded to be run only by the bootstrap node of the
        net. It initializes everything, successors, predecessor, successor list,
        backward fingers, finger table etc.
        """
        self._on_cycle = True
        self._disconnected = False
        for i in xrange(0, 160, 1):
            k = (self._local_url.hash_code() + 2**(i)) % 2**160
            self._references.update_finger_table(Key(k), self._local_url)

        ps_port = self._local_url.get_port()
        ps_host = self._local_url.get_host()
        self._references.set_predecessor(Url(ps_port, ps_host))
        self._references.set_successor(Url(ps_port, ps_host), 0)
        self._references.set_successor(Url(ps_port, ps_host), 1)
        for i in range(0, 3):
            self._references.update_back_fings(self._local_url)
            self._references.update_successor_list(self._local_url)
    #=========# BOOTSTRAP #=========#

    def join(self, bootstrap):
        """
        This is the function to call when a node wants to join the chord net.
        It needs the bootstrap Url.
        """
        resp = {}
        resp = self.iterative_find_successor(self._local_url, bootstrap)

        while not resp["on_cycle"]:
            url_obj = str_to_utl(resp["node_url"])
            resp = self.iterative_find_successor(bootstrap, url_obj)

        url_obj = str_to_url(resp["node_url"])

        self._references.set_successor(url_obj, 0)
        self._references.set_successor(url_obj, 1)

        # build fingers now
        self.build_fingers()
        self.strong_stabilize()
        self.fetch_successor_list()
        self.fetch_backwards_fings()
        self._disconnected = False

    def build_fingers(self):
        """
        This will build the finger table of the local node.
        """
        resp = {}
        my_suc = self._references.get_successor(0)
        my_hash = self._local_url.hash_code()

        for i in xrange(0, 160, 1):
            key_hash = (my_hash + 2**(i)) % 2**160
            if key_hash <= my_suc.hash_code() and key_hash > self._local_url.hash_code():
                url_obj = my_suc
            else:
                resp = self.iterative_find_successor(key_hash, my_suc)
                if resp is None:
                    return # need to do something here
                url_obj = str_to_url(resp["node_url"])
            self._references.update_finger_table(Key(key_hash), url_obj)

    def iterative_find_successor(self, source, dest):
        """
        Implementation of find successor rpc in an iterative fashion.
        Checks if the response is resolved, that means if we actually have
        the node Url we want or a predecessor of a node close to what we
        want.
        """
        if isinstance(dest, str):
            dest = str_to_url(dest)
        if not self.__find_successor(source, dest):
            return

        while len(self._buffer) == 0:
            continue

        resp = {}
        resp = self._buffer.pop()

        while not resp["resolved"]:
            url_obj = str_to_url(resp["node_url"])
            self.__find_successor(source, url_obj)
            while len(self._buffer) == 0:
                continue
            resp = self._buffer.pop()

        return resp

    def __find_successor(self, nid, dest):
        """
        This function is a rpc to a remote node asking for his successor.
        """

        channel = create_channel(dest)
        controller = create_controller(channel)
        request = service_pb2.FindSuccessorRequest()

        if isinstance(nid, Url):
            request.node_id = str(nid.hash_code())
        else: # nid is already a hash
            request.node_id = str(nid)

        service = service_pb2.FindSuccessorService_Stub(channel)
        service.FindSuccessor(controller, request, self.__callback)

        if controller.failed():
            log.debug("Rpc failed %s : %s" % (controller.error, controller.reason))
            return False
        else: return True

    def rmt_predecessor(self, dest_url):
        """
        This is responsible to get the predecessor of a remote node. It makes
        the rpc __remote_predecessor and then checks the _buffer for the
        response.
        """

        self.__remote_predecessor(dest_url)

        while len(self._buffer) == 0:
            continue

        resp = {}
        resp = self._buffer.pop()

        return resp

    def __remote_predecessor(self, dest_url):
        """
        This function is a rpc to a remote node asking for his predecessor.
        """

        channel = create_channel(dest_url)
        controller = create_controller(channel)
        request = service_pb2.PredecessorRequest()
        service = service_pb2.PredecessorService_Stub(channel)

        service.Predecessor(controller, request, self.__callback)

        if controller.failed():
            log.debug("[SimController] Rpc failed %s : %s" % (controller.error, controller.reason))

    def fetch_successor_list(self):
        """
        This function is responsible for contructing the successor list stored
        in References.
        """

        my_suc = self._references.get_successor(0)
        # [0]

        result = self.rmt_successor(my_suc)

        if result is None or len(result) == 0:
            return
        else:
            self._references.update_successor_list(str_to_url(result["sucs_suc"]))

            for i in range(0, 2):
                result = self.rmt_successor(str_to_url(result["sucs_suc"]))
                if result is None or len(result) == 0:
                    return
                self._references.update_successor_list(str_to_url(result["sucs_suc"]))

    def fetch_backwards_fings(self):
        """
        This function is responsible for contructing the backward fingers stored
        in References.
        """

        my_pred = self._references.get_predecessor()

        if my_pred is None:
            return

        result = self.rmt_predecessor(my_pred)

        if result["pred_url"] == "None" or len(result) == 0:
            return
        else:
            self._references.update_back_fings(str_to_url(result["pred_url"]))

            for i in range(0, 2):
                result = self.rmt_predecessor(str_to_url(result["pred_url"]))
                if result["pred_url"] == "None" or len(result) == 0:
                    return
                else:
                    self._references.update_back_fings(str_to_url(result["pred_url"]))

    def rmt_successor(self, dest_url):
        """
        This is responsible to get the successor of a remote node. It makes
        the rpc __remote_successor and then checks the _buffer for the
        response.
        """

        is_done = self.__remote_successor(dest_url)

        if not is_done:
            return None

        while len(self._buffer) == 0:
            continue

        resp = {}
        resp = self._buffer.pop()

        return resp

    def __remote_successor(self, dest_url):
        """
        This function is a rpc to a remote node asking for his predecessor.
        """

        channel = create_channel(dest_url)
        controller = create_controller(channel)
        request = service_pb2.SuccessorRequest()
        service = service_pb2.SuccessorService_Stub(channel)

        service.Successor(controller, request, self.__callback)

        if controller.failed():
            log.debug("[SimController] Rpc failed %s : %s" % (controller.error, controller.reason))
            return False
        else: return True

    def strong_stabilize(self):
        """
        This function is the implementation of the strong stabilization
        algorithm as described in the Chord paper.
        """

        my_suc0 = self._references.get_successor(0) # Url
        my_suc1 = self._references.get_successor(1) # Url

        resp = {}
        resp = self.iterative_find_successor(self._local_url, my_suc0)

        if resp is None:
            return

        resp_url = str_to_url(resp["node_url"])

        if resp_url == my_suc0:
            self._on_cycle = True
        else:
            self._on_cycle = False

        if my_suc0 == my_suc1:
            if (resp_url > self._local_url and resp_url < my_suc1) or \
            (resp_url > self._local_url and resp_url > my_suc1) or \
            (resp_url < self._local_url and resp_url < my_suc1):
                self._references.set_successor(resp_url, 1)

        self.update_and_notify(0)
        self.update_and_notify(1)

    def update_and_notify(self, i):
        """
        This function is according to Chord paper
        """

        s = self._references.get_successor(i)
        x = self.rmt_predecessor(s)

        # x must be an Url object
        pred_url = str_to_url(x["pred_url"])

        is_new = False
        if  (pred_url > self._local_url and pred_url > s) or \
        (pred_url < self._local_url and pred_url < s) or \
        (pred_url < self._local_url and pred_url > s):
            is_new = self._references.set_successor(pred_url, i)
        elif self._local_url == self._references.get_successor(i):
            is_new = self._references.set_successor(pred_url, i)

        s = self._references.get_successor(i)
        self.notify(self._local_url, s)

        #if is_new:
            # Got a new successor and need to update the forward successor list
        self._queue_task.appendleft(self.fetch_successor_list)
        self._queue_task.appendleft(self.fetch_backwards_fings)


    def notify(self, nid, dest):
        """
        This function makes the rpc notify to a remote node and let him knows
        that the local node thinks that he's the predecessor of the remote
        """

        if not isinstance(nid, Url):
            raise TypeError("".join([str(nid.__class__), "not Url type"]))

        channel = create_channel(dest)
        controller = create_controller(channel)

        request = service_pb2.NotifyRequest()
        # nid is Url *ONLY*
        request.prob_predecessor = str(nid)

        service = service_pb2.NotifyService_Stub(channel)
        service.Notify(controller, request, self.__callback)

        if controller.failed():
            log.debug("[SimController] Rpc failed %s : %s" % (controller.error, controller.reason))
            # Assume the remote node is crashed, time to stabilize
        #else: log.debug("[SimController] Success!")

    def ping(self, dest_url):
        """
        This function makes the rpc ping to a remote node.
        """

        channel = create_channel(dest_url)
        controller = create_controller(channel)
        request = service_pb2.PingRequest()

        service = service_pb2.PingService_Stub(channel)
        service.Ping(controller, request, self.__callback)

        if controller.failed():
            log.debug("[SimController] Rpc failed %s : %s" % (controller.error, controller.reason))
            # Assume the remote successor node is crashed, time to stabilize
            return False
        else: return True

    def transfer_keys(self, dest = None):
        """
        Prepare to make a rpc to a remote node to transfer the keys that the
        local node is holding according to it position in the chord ring.
        If the rpc fails, assume the remote node is crashed, this function will
        be called targeting the *successor*, if he is not there, wait for a
        strong stabilization to make sure we have the correct *suc* node and
        then retry asking the keys again.
        This will be done at the chord interface level.
        """

        if dest is None:
            dest = self._references.get_successor(0)

        channel = create_channel(dest)
        controller = create_controller(channel)
        request = service_pb2.TransferKeysRequest()
        res_list = self._stored_keys.remove_all()

        for elem in res_list:
            e = request.element.add()
            e.key_hash = str(elem[0])
            e.key_value = str(elem[1])

        service = service_pb2.TransferKeysService_Stub(channel)
        service.TransferKeys(controller, request, self.__callback)

        if controller.failed():
            log.debug("[SimController] Rpc failed %s : %s" % (controller.error, controller.reason))
            return False
        else: return True

    def ask_keys(self, dest = None):
        """
        Prepare to make a rpc to a remote node asking for the keys that must be
        stored by the local node according to its position in the chord ring.
        If the rpc fails, assume the remote node is crashed. This function will
        be called targeting the *predecessor* AND the *predecessor*, if he is
        not there, wait for a strong stabilization to make sure we have the
        correct targeting node and then retry asking the keys again.
        This will be done at the chord interface level.
        """

        if dest is None:
            dest = self._references.get_predecessor()

        channel = create_channel(dest)
        controller = create_controller(channel)

        request = service_pb2.AskKeysRequest()
        request.asker_url = str(self._local_url)

        service = service_pb2.AskKeysService_Stub(channel)
        service.AskKeys(controller, request, self.__callback)

        if controller.failed():
            log.debug("[SimController] Rpc failed %s : %s" % (controller.error, controller.reason))
            return False
        else: return True

    def search_key(self, key_hash, dest):
        channel = create_channel(dest)
        controller = create_controller(channel)

        request = service_pb2.SearchRequest()
        request.search_term = str(key_hash)

        service = service_pb2.SearchService_Stub(channel)
        service.Search(controller, request, self.__callback)

        if controller.failed():
            log.debug("[SimController] Rpc failed %s : %s" % (controller.error, controller.reason))
            return False
        else: return True

    def validate_position(self):
        my_suc0 = self._references.get_successor(0)
        my_suc1 = self._references.get_successor(1)

        if my_suc0 != my_suc1:
            return False

        my_pred = self._references.get_predecessor()

        if my_suc0 is None or my_suc1 is None or my_pred is None:
            return False
        elif my_suc0 == my_pred:
            return True

        sucs_pred = self.rmt_predecessor(my_suc0)
        preds_suc = suc_response = self.rmt_successor(my_pred)

        if sucs_pred == self._local_url and preds_suc == self._local_url:
            return True
        else:
            return False

    #==========# SIMULATION FUNCTIONS #==========#
    def percent_crash(self):
        total_nodes = len(self._remote_urls_list)
        x = (2 * total_nodes) / 10 # int
        to_crash = []
        to_crash = random.sample(self._remote_urls_list, x)

        for i in to_crash:
            if to_crash[i] in self._remote_urls_list:
                self._remote_urls_list.remove(to_crash[i])
            print "I'll crash", str(to_crash[i])
            self.crash_remote_node(to_crash[i])



    def percent_leave(self):
        total_nodes = len(self._remote_urls_list)
        x = (2 * total_nodes) / 10 # int
        to_crash = []
        to_crash = random.sample(self._remote_urls_list, x)

        for i in to_crash:
            if to_crash[i] in self._remote_urls_list:
                self._remote_urls_list.remove(to_crash[i])
            self.crash_remote_node(to_crash[i])

    def crash_remote_node(self, dest):
        channel = create_channel(dest)
        controller = create_controller(channel)

        request = simulation_service_pb2.CrashRequest()

        service = simulation_service_pb2.CrashService_Stub(channel)
        service.Crash(controller, request, self.__callback)

        if controller.failed():
            log.debug("[SimController] Rpc failed %s : %s" % (controller.error, controller.reason))
            return False
        else: return True

    def leave_remote_node(self, dest):
        channel = create_channel(dest)
        controller = create_controller(channel)

        request = simulation_service_pb2.LeaveRequest()

        service = simulation_service_pb2.LeaveService_Stub(channel)
        service.Leave(controller, request, self.__callback)

        if controller.failed():
            log.debug("[SimController] Rpc failed %s : %s" % (controller.error, controller.reason))
            return False
        else: return True

    def crash_local_node(self):
        sys.exit()

    def leave_chord(self):
        self.transfer_keys()
        self._scheduler.StopAllTasks()
        self._disconnected = True
        self._endpoint.shutdown()
        sys.exit()
    #==========# SIMULATION FUNCTIONS #==========#


def create_channel(dest_url):
    """
    Helper function to contruct and return a SocketRpcChannel object used by rpcs.
    """

    if not isinstance(dest_url, Url):
        raise TypeError("".join([str(dest_url.__class__), "not Url type"]))

    hostname = dest_url.get_host()
    port = dest_url.get_port()

    channel = protobuf.channel.SocketRpcChannel(hostname, port)

    return channel


def create_controller(channel):
    """
    Helper function to contruct and return a Controller object used by rpcs
    """

    controller = channel.newController()
    return controller


def str_to_url(str_url):
    """
    Helper function that accepts a Url formatted as a string and returns an
    Url object. It is used mostly when we have a response from a rpc where
    the Url is stored as a string.
    """

    part = str_url.partition(':')
    url_obj = Url(int(part[2]), part[0])
    return url_obj



############################################################
if __name__ == "__main__":
    from hashlib import sha1
    m = SimController(3000)
    m.init_bootstrap()
    m._endpoint.run_endpoint()

    for i in range(0, 10):
        str_i = str(i) + ' : ' + 'a'*(i+1)
        m._stored_keys.insert(Key(long(sha1(str(i)).hexdigest(), 16)), str_i)
    i = 10
    m._stored_keys.insert(Key(long(sha1(str(i)).hexdigest(), 16)), "Archi: I'm happy")
    print m._stored_keys.has_key(long(sha1('0').hexdigest(), 16))
    print m._stored_keys.has_key(long(sha1('10').hexdigest(), 16))
############################################################
