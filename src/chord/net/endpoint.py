#!/usr/bin/python2.6
#
# This file is part of ErnChord
#
# ErnChord is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) 2010 Vourlakis Nikolas

"""
Module chord/net/endpoint.py : The rpc server running on a node
Classes : Endpoint
Status: READY && TESTED
"""

__author__ = "nvourlakis@gmail.com (Vourlakis Nikolas)"
__version__ = "1.0"


# Standard module imports
import threading
import logging

# 3rd party imports
#from protobuf import *
from protobuf.server import SocketRpcServer
from protobuf.server import ThreadedTCPServer
from protobuf.server import SocketHandler


log = logging.getLogger("__name__")


class Endpoint(SocketRpcServer):
    """
    Class Endpoint

    Endpoint is a subclass of protobuf.server.SocketRpcServer. It creates a
    separate thread for the rpc server to be run and control its life.
    Before starting the server, use registerService(service) to register the
    rpc services.
    +==============================================+
    IMPORTANT: REQUIRES Python2.6+ to be run correctly and that's cause of
    function BaseServer.shutdown()
    +==============================================+
    """

    def __init__(self,  port,  host='localhost'):
        SocketRpcServer.__init__(self, port, host)
        self.port = port
        self.host = host
        log.debug("[Endpoint] Creating rpc server object on %s:%s" % (self.host, self.port))
        self._server = ThreadedTCPServer((self.host, self.port), SocketHandler)
        self._thread = threading.Thread(target = self.run)

    def shutdown(self):
        """
        Function shutdown: Sets the server stop condition
        NOTE : This must be called after server_forever() has started and while it's
        running on another thread, how I imlemented here that is.
        """

        log.info("Rpc server shuting down")
        self._server.shutdown()


    def run_endpoint(self):
        """
        Function run_endpoint: The function to call to start the threaded
        rpc server.
        """

        self._thread.start()


    def run(self):
        """
        Overloaded function
        Activate the server. The function to be run be the server thread
        """

        log.info("Running rpc server on %s:%s" % (self.host, self.port))
        self._server.serve_forever()
