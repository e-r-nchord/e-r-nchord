#!/usr/bin/python2.6
# This file is part of ErnChord
#
# ErnChord is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) 2010 Vourlakis Nikolas

"""
Module chord/net/stored_keys.py: Represents the data stored by a node
Classes: StoredKeys
Status: READY & TESTED
"""

__author__ = "nvourlakis@gmail.com (Vourlakis Nikolas)"
__version__ = "1.0"


# Standard module imports
import logging

# self-defined module imports
from data.url import Url

# protoc generated module import
import simulation_service_pb2


log = logging.getLogger("__name__")


class CrashService(simulation_service_pb2.CrashService):
    """
    Controller only rpc. The purpose is to send signals to selected nodes
    and make them crash...because we are evil!
    """

    def __init__(self, parent):
        self._parent = parent

    def Crash(self, controller, request, done):
        log.debug("!!! PREPARING TO CRASH !!!")
        response = simulation_service_pb2.CrashResponse()
        self._parent.crash_local_node()
        #self._parent._queue_task.appendleft(self._parent.crash_local_node)
        done.run(response)

class LeaveService(simulation_service_pb2.LeaveService):
    """
    Controller only rpc. The purpose is to send signals to selected nodes
    and make them leave the network normally (transfering keys, etc)
    """

    def __init__(self, parent):
        self._parent = parent

    def Leave(self, controller, request, done):
        log.debug("!!! PREPARING TO LEAVE !!!")
        response = simulation_service_pb2.LeaveResponse()
        self._parent._queue_task.appendleft(self._parent.leave_chord)
        done.run(response)


class MyUrlService(simulation_service_pb2.MyUrlService):
    """
    Controller only rpc. A remote node sends its url to be stored by the
    controller in a list. That list will be used to select random nodes and
    crash them!
    """

    def __init__(self, parent):
        self._parent = parent

    def SendMyUrl(self, controller, request, done):
        print "SendMyUrl", request
        remote_url = str_to_url(request.my_url)
        self._parent._remote_urls_list.append(remote_url)
        response = simulation_service_pb2.MyUrlResponse()
        done.run(response)



def str_to_url(str_url):
    """
    Helper function that accepts a Url formatted as a string and returns an
    Url object. It is used mostly when we have a response from a rpc where
    the Url is stored as a string.
    """

    part = str_url.partition(':')
    url_obj = Url(int(part[2]), part[0])
    return url_obj
