#!/usr/bin/python2.6
# This file is part of ErnChord
#
# ErnChord is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) 2010 Vourlakis Nikolas

"""
Module chord/net/references.py: Key representation
Classes: Borg, References
Status: READY & TESTED
"""

__author__ = "nvourlakis@gmail.com (Vourlakis Nikolas)"
__version__ = "1.0"


# Standard module imports
import logging

# Self-defined module imports
from fingertable import FingerTable
from entry import Entry
from data.url import Url
from rwlock import ReadWriteLock


log = logging.getLogger("__name__")


class Borg:
    """
    Class Borg

    This class ensures that multiple References objects share state.
    """

    _shared_state = {}
    def __init__(self):
        self.__dict__ = self._shared_state



class References(Borg):
    """
    Class References

    Holds references to the finger table, the successor list and the _predecessor
    and it's responsible to update them also. Everything is protected by a
    reader/writer lock for thread safety
    """

    def __init__(self, local_url):
        Borg.__init__(self)
        if not isinstance(local_url, Url):
            raise TypeError("".join([str(local_url.__class__), "not Url type"]))

        self._local_url = local_url
        self._fingers = FingerTable(local_url)
        self._predecessor = None
        self._successor = [None]*2
        # _successor_list contains 3 successor entries starting from
        # the successor's successor of this node
        # [0] = successor's successor and so on
        # [0] is the closest
        self._successor_list = [None]*3
        # back_fings contains 3 predecessor entries starting from the
        # predecessor's precessor
        # [0] = predecessor's predecessor
        # [0] is the closest
        self._back_fings = [None]*3
        self._lock = ReadWriteLock()

    def update_back_fings(self, url_obj):
        self._lock.acquireWrite()
        try:
            self._back_fings[0:2] = self._back_fings[1:]
            self._back_fings[2] = url_obj
        finally:
            self._lock.release()

    def get_back_fings(self):
        figs = []
        self._lock.acquireRead()
        try:
            figs = self._back_fings
        finally:
            self._lock.release()
        return figs

    def update_successor_list(self, url_obj):
        self._lock.acquireWrite()
        try:
            self._successor_list[0:2] = self._successor_list[1:]
            self._successor_list[2] = url_obj
        finally:
            self._lock.release()

    def get_successor_list(self):
        suc_list = []
        self._lock.acquireRead()
        try:
            suc_list = self._successor_list
        finally:
            self._lock.release()

        return suc_list

    def update_finger_table(self, key_ref, url_ref):
        """ Accepts a key and an url and updates the fingers. """

        self._lock.acquireWrite()
        try:
            entry_ref = Entry(key_ref, url_ref)
            self._fingers.update_fingers(entry_ref)
        finally:
            self._lock.release()

    def search_finger_table(self, node_id):
        """ Search the finger table for a node. """

        self._lock.acquireRead()
        try:
            res = self._fingers.closest_preceding_node(node_id)
        finally:
            self._lock.release()

        return res # Url

    def set_predecessor(self, pre_url):
        """ Set a new predecessor. """
        result = False
        if not isinstance(pre_url, Url):
            raise TypeError("".join([str(pre_url.__class__), "not Url type"]))

        self._lock.acquireWrite()
        try:
            if self._predecessor is None or self._predecessor != pre_url:
                self._predecessor = pre_url
                result = True
        finally:
            self._lock.release()

        return result

    def get_predecessor(self):
        """ Get current predecessor. """

        self._lock.acquireRead()
        try:
            pred = self._predecessor
        finally:
            self._lock.release()

        return pred

    def set_successor(self, suc_url, i):
        """
        Set a new successor, the 2nd argument is based on the strong
        stabilization algorithm.
        """

        result = False

        if not isinstance(suc_url, Url):
            raise TypeError("".join([str(suc_url.__class__), "not Url type"]))
        self._lock.acquireWrite()
        try:
            if i == 0 or i == 1:
                if self._successor[i] is None or self._successor[i] != suc_url:
                    self._successor[i] = suc_url
                    result = True
                    if not self._fingers.is_empty():
                        self._fingers.fix_successors_fingers(suc_url)
            else:
                log.error("[References] Invalid argument,0 <= i <= 1")
        finally:
            self._lock.release()

        return result

    def get_successor(self, i):
        """ Returns the successor list. """

        self._lock.acquireRead()
        try:
            suc = self._successor[i]
        finally:
            self._lock.release()

        return suc

##############> Debug Functions <##############
    def get_fingers(self): # DEBUG purposes only!!!!!!!!!!
        self._lock.acquireRead()
        try:
            ft = str(self._fingers)
        finally:
            self._lock.release()

        return ft

    def print_fingers(self): # DEBUG purposes only!!!!!!!!!!
        self._fingers.print_fingertable()
