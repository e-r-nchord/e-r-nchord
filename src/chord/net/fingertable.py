#!/usr/bin/python2.6
# This file is part of ErnChord
#
# ErnChord is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) 2010 Vourlakis Nikolas

"""
Module chord/net/fingertable.py: Key representation
Classes: FingerTable
Status: ALMOST READY & TESTED
"""

__author__ = "nvourlakis@gmail.com (Vourlakis Nikolas)"
__version__ = "1.0"


# Standard module imports
import logging

# Self-defined library imports
from entry import Entry
from data.url import Url
from data.key import Key


log = logging.getLogger("__name__")


class FingerTable(object):
    """
    Class FingerTable

    Represents the finger table of a node in the chors ring. It stores Entry
    objects. Also here is the implementation of the closest_preceding_node
    function based on the Chord paper
    """

    def __init__(self, local_url):
        if not isinstance(local_url, Url):
            raise TypeError("".join([str(local_url.__class__), " not Url type"]))
        self._local_url = local_url
        self._fingers = []
        self._maxfings = 160

# DEBUG
    def print_fingertable(self):
        for i in range(0, 160, 1):
            print self._fingers[i]
# DEBUG

    def __str__(self):
        str_rep = ""
        for elem in self._fingers:
            str_rep = "".join([str_rep, '\n', str(elem)])
        return str_rep
        #return "".join([str(self._fingers)])

    def is_empty(self):
        return len(self._fingers) == 0

    def update_fingers(self, entry_ref):
        """
        Keeps adding Entry objects till 160 are inserted. Every call of this
        function after that point will update an Entry key to a new url.
        """

        if not isinstance(entry_ref, Entry):
            raise TypeError("".join([str(entry_ref.__class__), "not Entry type"]))

        if len(self._fingers) <= self._maxfings and entry_ref not in self._fingers:
            self._fingers.append(entry_ref)
        else:
            for index in xrange(len(self._fingers)):
                if self._fingers[index].get_key() == entry_ref.get_key():
                    self._fingers[index] = entry_ref

    def fix_successors_fingers(self, url_obj):
        for index in xrange(0, 160, 1):
            if url_obj.hash_code() >= self._fingers[index].get_key().hash_code() \
            and self._local_url.hash_code() < self._fingers[index].get_key().hash_code():
                self._fingers[index].update_url_ref(url_obj)


    def closest_preceding_node(self, n_id):
        """
        Searches the finger table backwards till it finds a key so that the
        searched id is between that key and the hash of the node of this
        fingertable url.
        """

        # NOTE: If the key is stored in the finger it will not be returned
        # It will be returned the predecessor of the node that holds that key
        # based on the Chord paper

        #local = Key(str(self._local_url.hash_code()))
        local = self._local_url.hash_code()
        for index in reversed(xrange(0, 160, -1)):
            if self._fingers[index].get_key().hash_code() > local.hash_code() and \
                self._fingers[index].get_key().hash_code() < n_id:
                # we found the predecessor
                return self._fingers[index].get_url()

        return self._local_url
