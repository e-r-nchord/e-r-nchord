#!/usr/bin/python2.6
# This file is part of ErnChord
#
# ErnChord is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) 2010 Vourlakis Nikolas

"""
Module chord/net/stored_keys.py: Represents the data stored by a node
Classes: StoredKeys
Status: READY & TESTED
"""

__author__ = "nvourlakis@gmail.com (Vourlakis Nikolas)"
__version__ = "1.0"


# self-defined module imports
from data.key import Key
from rwlock import ReadWriteLock


class StoredKeys(object):
    """
    Class StoredKeys

    Represents the actual data stored by a node. Each entry contains a hash
    and the real value.
    """

    def __init__(self):
        # list with all the keys stored [ [key_hash, key_value], ]
        # key_hash is a Key object
        self._keys_stored = []
        self._lock = ReadWriteLock()

    def insert(self, key_hash, key_value):
        """
        Insert a new pair in the list of stored keys.
        """

        if not isinstance(key_hash, Key):# and isinstance(key_hash, str):
            key_hash = Key(key_hash)
        to_insert = [key_hash, key_value]

        self._lock.acquireWrite()
        try:
            self._keys_stored.append(to_insert)
        finally:
            self._lock.release()

    def remove_all(self):
        """
        Removes every single entry in the list. This will be called when
        transfering keys to a remote node cause of leaving normally
        the chord ring.
        """

        self._lock.acquireWrite()
        try:
            res = self._keys_stored[:]
            del self._keys_stored[:]
        finally:
            self._lock.release()

        return res

    def remove(self, start, end):
        """
        Given a start hash and an end hash return all the keys as a list with
        hash > start and hash < end.
        """

        result = []

        self._lock.acquireWrite()
        try:
            # FIXME: This is a dirty filthy way to do
            # case 1: start < end, trivial
            if start > end:
                for i in xrange(0, len(self._keys_stored)):
                    if (self._keys_stored[i][0].hash_code() > start and \
                    self._keys_stored[i][0].hash_code() > end) or\
                    self._keys_stored[i][0].hash_code() <start and\
                    self._keys_stored[i][0].hash_code() < end:
                        el = self._keys_stored[i]
                        result.append(el)
            # case 2: start > end
            elif start < end:
                for i in xrange(0, len(self._keys_stored)):
                    if (self._keys_stored[i][0].hash_code() > start) or \
                    (self._keys_stored[i][0].hash_code() < start and \
                    self._keys_stored[i][0].hash_code() < end):
                        el = self._keys_stored[i]
                        result.append(el)

            for i in result:
                self._keys_stored.remove(i)
        finally:
            self._lock.release()

        return result # list

    def is_empty(self):
        """ Returns True if the list with stored keys is empty. """

        return len(self._keys_stored) == 0

    def has_key(self, search_key):
        """
        Searches for a key hash in the stored list and if it finds it
        then it returns the corresponding key_value.
        """

        result = None

        #if not isinstance(search_key, Key):
         #   search_key = Key(search_key)

        self._lock.acquireRead()
        try:
            for key_hash, key_value in self._keys_stored:
                if key_hash.hash_code() == search_key:

                    result = key_value
                    break
        finally:
            self._lock.release()

        return result

    def print_keys(self): # DEBUG ONLY, TO BE REMOVED
        for key_hash, key_value in self._keys_stored:
            print str(key_hash), key_value
