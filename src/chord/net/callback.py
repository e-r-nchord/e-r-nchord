#!/usr/bin/python2.6
# This file is part of ErnChord
#
# ErnChord is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) 2010 Vourlakis Nikolas

"""
Module chord/net/callback.py:
Classes: CallBack
Status: READY & ESTED
"""

__author__ = "nvourlakis@gmail.com (Vourlakis Nikolas)"
__version__ = "1.0"


# Standard module imports
import logging

# protoc generated module import
import service_pb2
import simulation_service_pb2


log = logging.getLogger("__name__")


class Callback:
    """
    Class Callback

    This class is the the one to pass to the rpcs services when asking
    a remote node. This class defines how the response will be manipulated.
    Needed according to module protobuf-rpc-socket
    """

    def __init__(self, parent):
        # Reference to Master
        self._parent = parent

    def run(self, response):
        """
        The function that will examine the respons and make the appropriate action
        """

        if isinstance(response, service_pb2.FindSuccessorResponse):
            resp_list = response.ListFields()
            resp_dict = {}

            for descr, value in resp_list:
                resp_dict[descr.name] = value

            self._parent._buffer.appendleft(resp_dict)
        elif isinstance(response, service_pb2.PredecessorResponse):
            resp_list = response.ListFields()
            resp_dict = {}

            for descr, value in resp_list:
                resp_dict[descr.name] = value

            self._parent._buffer.appendleft(resp_dict)
        elif isinstance(response, service_pb2.SuccessorResponse):
            resp_dict = {}

            for descr, value in response.ListFields():
                resp_dict[descr.name] = value

            self._parent._buffer.appendleft(resp_dict)
        elif isinstance(response, service_pb2.AskKeysResponse):
            for elem in response.element:
                self._parent._stored_keys.insert(str(elem.key_hash), str(elem.key_value))

        elif isinstance(response, service_pb2.SearchResponse):
            resp_list = response.ListFields()
            resp_dict = {}

            for descr, value in resp_list:
                resp_dict[descr.name] = value

            self._parent._search_result.appendleft(resp_dict)
