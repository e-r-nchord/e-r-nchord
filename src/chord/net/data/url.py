#!/usr/bin/python2.6
#
# This file is part of ErnChord
#
# ErnChord is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) 2010 Vourlakis Nikolas

"""
Module chord/net/data/url.py : Node address information
Classes : Url
Status: READY && TESTED
"""

__author__ = "nvourlakis@gmail.com (Vourlakis Nikolas)"
__version__ = "1.0"


# standart library imports
import hashlib


class Url(object):
    """
    Class Url

    Url represents the <host:port> of a node.
    """

    def __init__(self,  port,  host='localhost'):
        self._port = port
        self._host = host
        self._str_repr = "".join([self._host, ":",  str(self._port)])
        self._hash = long(hashlib.sha1(self._str_repr).hexdigest(),  16)

    def __str__(self):
        return self._str_repr

    def __cmp__(self, obj):
        if not isinstance(obj, Url):
            raise TypeError("".join([str(obj.__class__), " not Url type"]))
        else:
            if self.hash_code() > obj.hash_code():
                return 1
            elif self.hash_code() == obj.hash_code():
                return 0
            else:
                return -1

    def get_host(self):
        """ return stringed host """

        return self._host # str

    def get_port(self):
        """ return stringed port """

        return self._port # str

    def hash_code(self):
        """ Returns the sha1 hash as a long number """
        return self._hash # long
