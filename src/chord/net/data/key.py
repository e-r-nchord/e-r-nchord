#!/usr/bin/python2.6
# This file is part of ErnChord
#
# ErnChord is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) 2010 Vourlakis Nikolas

"""
Module chord/net/data/key.py: Key representation
Classes: Key
Status: READY && TESTED
"""

__author__ = "nvourlakis@gmail.com (Vourlakis Nikolas)"
__version__ = "1.0"

# standart library imports
from hashlib import sha1


class Key(object):
    """
    Class Key

    Represents a Key stored in the chord ring
    """

    def __init__(self,  str_key):
        self._key = str_key
        if not isinstance(str_key, long):
            self._hash_code = long(str_key)#, 16)
        else:
            self._hash_code = str_key

    def __str__(self):
        #"[Key: ",  self._key,  "]"])
        #return "".join([self._key])
        return str(self._hash_code)

    def __getattr__(self, name):
        if name == 'key':
            return self._key
        elif name == 'hash':
            return self.hash_code()
        else:
            raise AttributeError,  name

    def __eq__(self,  obj):
        if not isinstance(obj, Key):
            raise TypeError, "".join([str(obj.__class__), " not Key type"])

        return self.hash_code() == obj.hash_code()

    def __cmp__(self,  obj):
        # compares the key hashes
        if not isinstance(obj, Key):
            raise TypeError, "".join([str(obj.__class__), " not Key type"])

        if self.hash_code() > obj.hash_code():
           return 1
        elif self.hash_code() == obj.hash_code():
            return 0
        else:
            return -1

    def hash_code(self):
        """ Return the sha1 of the key as a long number. """

        return self._hash_code # long

    def update_key(self,  str_key):
        """ Change the strored key. """

        self._key = self._hash_code
