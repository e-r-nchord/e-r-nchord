#!/usr/bin/python2.5
#
# This file is part of ErnChord
#
# ErnChord is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) 2010 Vourlakis Nikolas

"""
Module chord/net/entry.py : Represents an entry in the fingertable
Classes : Entry
Status: READY && TESTED
"""

__author__ = "nvourlakis@gmail.com (Vourlakis Nikolas)"
__version__ = "1.0"


# self-defined library imports
from data.key import Key
from data.url import Url


class Entry(object):
    """
    Class Entry

    Represents a tuple of <key : (url,on_cycle)>, that is an association of a
    key with the a node url in chord ring that has info about that key.
    """

    def __init__(self, key_obj, url_obj):
        if not isinstance(url_obj, Url):
            raise TypeError("".join([str(url_obj.__class__), " not Url type"]))
        if not isinstance(key_obj, Key):
            raise TypeError("".join([str(key_obj.__class__), " not Key type"]))

        self._key = key_obj
        self._url = url_obj

    def __str__(self):
        return "".join( [str(self._key.hash_code()), " : ", str(self._url)] )

    def update_url_ref(self, url_ref):
        """
        Updates a single key to a new node url
        """

        if not isinstance(url_ref, Url):
            raise TypeError("".join([str(url_ref.__class__), " not Url type"]))
        else:
            self._url = url_ref

    def update_key_ref(self, key_ref):
        """
        Updates with a new key preserving the node url
        Porbably not usable
        """

        if not isinstance(key_ref, Key):
            raise TypeError("".join([str(key_ref.__class__), " not Key type"]))
        else:
            self._key = key_ref

    def get_key(self):
        """ Returns the key stored """

        return self._key

    def get_url(self):
        """ Returns the url stored """

        return self._url

    def __eq__(self, obj):
        if not isinstance(obj, Entry):
            raise TypeError("".join([str(obj.__class__), " not Entry type"]))
        #print '__eq__ in Entry'
        if self.get_key() == obj.get_key() and self.get_url() == obj.get_url():
            return True
        return False

