#!/usr/bin/python2.6
#
# This file is part of ErnChord
#
# ErnChord is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) 2010 Vourlakis Nikolas

"""
Module chord/net/rpc_services.py : Implementation of the rpcs
Classes : FindSuccessorService, NotifyService, PredecessorService, PingService
Functions: str_to_url
Status: READY && NEEDS A LOT OF TESTING
"""

__author__ = "nvourlakis@gmail.com (Vourlakis Nikolas)"
__version__ = "1.0"


# Standard module imports
import logging
from hashlib import sha1

# self-defined module imports
from references import References
from fingertable import FingerTable
from data.url import Url
from data.key import Key

# protoc generated module import
import service_pb2


log = logging.getLogger("__name__")


class FindSuccessorService(service_pb2.FindSuccessorService):
    """
    Class FindSuccessorService

    Rpc server side implementation of find_successor function as
    described in the Chord paper. There are 3 situations that we take
    into account to form the response.
    a) If the master is the bootstrap node of the Chord ring then he is
    our default successor.
    b) Else if the node url is between the local node url and the local's
    successor node then that node is our successor.
    c) Else if none of the above qualifies then we search the finger table,
    select the farthest node from the one we are searching and then
    refer to the Master to make a new rpc find_successor to the that node.
    """

    def __init__(self, parent):
        # reference to References
        self._parent = parent

    def FindSuccessor(self, controller, request, done):
        # request.node_id (string), this is a sha1 hash
        response = service_pb2.FindSuccessorResponse()
        response.resolved = False

        node_hash = long(request.node_id, 16)

        if  node_hash > self._parent._local_url.hash_code() and \
        node_hash <= self._parent._references.get_successor(0).hash_code():
            response.node_url = str(self._parent._local_url)
            response.on_cycle = self._parent._on_cycle
            response.resolved = True
        else:
            # ret is a Url object
            ret = self._parent._references.search_finger_table(node_hash)
            response.node_url = str(ret)

            if ret == self._parent._local_url:
                # check in case this is one of my keys
                response.on_cycle = True
                response.resolved = True
            else:
                response.resolved = False

        done.run(response)




class NotifyService(service_pb2.NotifyService):
    """
    Class NotifyService

    Gets called after a strong stabilization followed by an
    update_and_notify. The node passed as the request thinks that he is
    out predecessor.
    """

    def __init__(self, parent):
        # reference to Master
        self._parent = parent

    def Notify(self, controller, request, done):
        req = str_to_url(request.prob_predecessor)
        my_pred = self._parent._references.get_predecessor()

        is_new = False

        if my_pred is None:
            is_new = self._parent._references.set_predecessor(req)
        elif  (req > my_pred and req > self._parent._local_url) or \
        (req < my_pred and req < self._parent._local_url) or \
        (req < my_pred and req > self._parent._local_url):
            is_new = self._parent._references.set_predecessor(req)
        elif my_pred == self._parent._local_url:
            # This is for the bootstrap node
            is_new = self._parent._references.set_predecessor(req)

        if is_new:
            self._parent._queue_task.appendleft(self._parent.fetch_backwards_fings)
            self._parent._queue_task.appendleft(self._parent.strong_stabilize)
        elif req != my_pred:
            self._parent._queue_task.appendleft(self._parent.strong_stabilize)

        response = service_pb2.NotifyResponse()
        done.run(response)





class PredecessorService(service_pb2.PredecessorService):
    """
    Class PredecessorService

    Rpc server side implementation retrieval of the predecessor of a remote node,
    this class is derived from the abstract class produced by the google
    protocol buffer compiler
    """

    def __init__(self, parent):
        # reference to Master
        self._parent = parent

    def Predecessor(self, controller, request, done):
        response = service_pb2.PredecessorResponse()
        response.pred_url = str(self._parent._references.get_predecessor())
        done.run(response)


class SuccessorService(service_pb2.SuccessorService):
    """
    Class PredecessorService

    Rpc server side implementation retrieval of the predecessor of a remote node,
    this class is derived from the abstract class produced by the google
    protocol buffer compiler
    """

    def __init__(self, parent):
        # reference to Master
        self._parent = parent

    def Successor(self, controller, request, done):
        response = service_pb2.SuccessorResponse()
        response.sucs_suc = str(self._parent._references.get_successor(0))
        done.run(response)


class PingService(service_pb2.PingService):
    """
    Class PingService

    Ping rpc server side implementation, this class is derived from the abstract
    class produced by the google protocol buffer compiler
    """

    def Ping(self, controller, request, done):
        response = service_pb2.PingResponse()
        done.run(response)


class TransferKeysService(service_pb2.TransferKeysService):
    """
    Class TransferKeysService

    TransferKeysService rpc server side  implementation. This rpc is triggered
    when a node is preparing to leave the cord ring and must transfer his keys
    to its successor.
    """

    def __init__(self, parent):
        self._parent = parent

    def TransferKeys(self, controller, request, done):
        for elem in request.element:
            self._parent._stored_keys.insert(elem.key_hash, elem.key_value)

        response = service_pb2.TransferKeysResponse()

        done.run(response)


class AskKeysService(service_pb2.AskKeysService):
    """
    Class AskKeysService

    AskKeysService rpc server side implementation. This rpc is triggered when a
    node has already joined the chord ring and found his successor. He must ask
    his remote successor for the keys that must be stored by him.
    """

    def __init__(self, parent):
        self._parent = parent

    def AskKeys(self, controller, request, done):
        asker_url = str_to_url(request.asker_url)
        res_list = []

        res_list = self._parent._stored_keys.remove(self._parent._local_url.hash_code(), asker_url.hash_code())

        response = service_pb2.AskKeysResponse()

        if len(res_list) != 0:
            for element in res_list:
                e = response.element.add()
                e.key_hash = str(element[0])
                e.key_value = str(element[1])

        done.run(response)


class SearchService(service_pb2.SearchService):
    """
    Class SearchService

    SearchService rpc server side implementation. Given a hash first the node
    searches the local stored keys. If we got a hit we return the value or else
    the node searches in his finger table (problematic) and returns the closest
    preceding node.
    At the moment,it returns the successor due to a problem arising the finger
    table not updated correctly and so is back in "history".
    """

    def __init__(self, parent):
        self._parent = parent

    def Search(self, controller, request, done):

        response = service_pb2.SearchResponse()

        to_search = long(request.search_term)
        my_pred = self._parent._references.get_predecessor()

        # FIXME: Problem with getting the keys
        # Check to see if I hold the key
        if to_search > self._parent._local_url.hash_code() and \
        to_search > my_pred.hash_code():
            result = self._parent._stored_keys.has_key(to_search)
        # changed '>' to '<'
        elif to_search < self._parent._local_url.hash_code() and \
        to_search < my_pred.hash_code():
            result = self._parent._stored_keys.has_key(to_search)

        if result is None:
            #to_contact = self._parent._references.search_finger_table(to_search)
            to_contact = self._parent._references.get_successor(0)
            response.search_result = str(to_contact)
            response.resolved = False
        else:
            response.search_result = result
            response.resolved = True

        done.run(response)


def str_to_url(str_url):
    """
    Helper function that accepts a Url formatted as a string and returns an
    Url object. It is used mostly when we have a response from a rpc where
    the Url is stored as a string.
    """

    part = str_url.partition(':')
    url_obj = Url(int(part[2]), part[0])
    return url_obj
