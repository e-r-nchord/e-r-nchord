import sys
from hashlib import sha1

import src.chord.chord_interface as chord_interface


class Console(object):
    def __init__(self, port, hostname):
        self._ern_chord = chord_interface.ChordInterface(port, hostname)

    def consolse_run(self):
        while True:
            command = raw_input(">>>")
            command = str.split(command.encode())
            print len(command)
            if len(command) != 0:
                func = self.dispatch(command[0])
                func(*command[1:])

    def do_connect(self, port, hostname):
        self._ern_chord.connect(int(port), hostname)

    def do_disconnect(self):
        self._ern_chord.leave_network()

    def do_find(self, search):
        result = self._ern_chord.find(search)
        print result

    def do_quit(self):
        print "Stopping all tasks..."
        self._ern_chord.leave_network()
        del self._ern_chord
        print "ErnChord is shutting down. Bye bye!!!"
        sys.exit(0)

    def do_help(self):
        print "Command Help:"
        print "\tconnect <port> <ip>"
        print "\tfind <search_term>"
        print "\tdisconnect"
        print "\tquit"

    def error(self):
        print "Unknown command"
        self.do_help()

    def dispatch(self, command):
        print command
        mname = 'do_' + command
        if hasattr(self, mname):
            method = getattr(self, mname)
            return method
        else:
            return self.error


if __name__ == "__main__":
    print "I need the port and ip to run the server"
    cport = raw_input(">>> Specify the port:")
    chostname = raw_input(">>> Specify the ip:")
    c = Console(int(cport), chostname)
    c.consolse_run()
